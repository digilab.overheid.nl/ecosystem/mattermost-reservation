package main

import (
	"encoding/json"
	"fmt"
	"sort"
	"time"

	"github.com/google/uuid"
	"github.com/mattermost/mattermost/server/public/pluginapi"
)

const DBPrefix = "reservation-"

type Reservation struct {
	ID               uuid.UUID    `json:"id"`
	Room             string       `json:"room"`
	Start            AbsoluteTime `json:"reservation_start"`
	End              AbsoluteTime `json:"reservation_end"`
	Owner            string       `json:"owner"`
	ParticipantCount int          `json:"participant_count"`
	Description      string       `json:"description"`
	IsEditable       bool         `json:"is_editable"` // Whether or not this reservation can be edited/deleted by the current user (read only attribute, only applicable for fetching reservations)
}

// Define Reservations as a type, in order to be able to implement the sort.Interface on it
type Reservations []Reservation

type ReservationSet struct {
	Reservations Reservations
}

type ReservationList struct {
	Date  AbsoluteTime
	Rooms map[string]ReservationSet
}

// Implement the sort.Interface interface for Reservations
func (s Reservations) Len() int           { return len(s) }
func (s Reservations) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s Reservations) Less(i, j int) bool { return s[i].Start.Time < s[j].Start.Time } // Note: since AbsoluteTime is in RFC 3339 format, we can compare string values

// Check if the reservation data is valid
// - Start time is before end time
// - End time is in the future
// - Reservation is less than 24 hours
// - Reservation is at least 15 minutes
func (r *Reservation) CheckData() error {
	if r.Start.After(r.End) {
		return fmt.Errorf("start time is after end time")
	}

	if r.End.Before(AbsoluteTimeNow()) {
		return fmt.Errorf("end time is in the past")
	}

	if r.Start.FormatDate() != r.End.FormatDate() {
		return fmt.Errorf("reservation must start and end on the same day")
	}

	if r.End.Sub(r.Start) > time.Hour*24 {
		return fmt.Errorf("reservation must be less than 24 hours")
	}

	if r.End.Sub(r.Start) < time.Minute*15 {
		return fmt.Errorf("reservation must be at least 15 minutes")
	}

	return nil
}

func (p *Plugin) GetReservationList(date *AbsoluteTime, requester *AuthUser) (*ReservationList, error) {
	// Lock the reservation data
	p.reservationLock.Lock()
	defer p.reservationLock.Unlock()

	// Load the reservation data
	reservationList := ReservationList{
		Date:  *date,
		Rooms: make(map[string]ReservationSet),
	}

	for _, room := range p.roomConfig.Rooms {
		index := buildIndex(room.ID, date)

		reservationSet := ReservationSet{
			Reservations: Reservations{},
		}

		err := p.client.KV.Get(index, &reservationSet)
		if err != nil {
			return nil, err
		}

		// Set the IsEditable attribute for each reservation
		if requester.userType.IsEditor() {
			for i, reservation := range reservationSet.Reservations {
				reservationSet.Reservations[i].IsEditable = checkPermission(reservation, requester.Id, p.reservationAdmins) // Note: could be slightly improved by checking only once whether or not the user is an admin
			}
		}

		// Sort the reservations by start time
		sort.Sort(reservationSet.Reservations)

		reservationList.Rooms[room.ID] = reservationSet
	}

	return &reservationList, nil
}

func (p *Plugin) AddReservation(reservation *Reservation) error {
	// Lock the reservation data
	p.reservationLock.Lock()
	defer p.reservationLock.Unlock()

	// Check if the room exists
	valid := p.roomConfig.Contains(reservation.Room)
	if !valid {
		return fmt.Errorf("room not found")
	}

	// Check the reservation data
	err := reservation.CheckData()
	if err != nil {
		return fmt.Errorf("could not check reservation validity: %w", err)
	}

	// Check for overlapping reservations
	hasOverlap, err := p.checkOverlap(reservation, nil)
	if err != nil {
		return fmt.Errorf("error checking for overlapping reservations: %w", err)
	}
	if hasOverlap {
		return fmt.Errorf("Reservation overlaps with existing reservation")
	}

	// Add the reservation
	return p.addReservation(reservation)
}

func (p *Plugin) RemoveReservation(room string, date *AbsoluteTime, id uuid.UUID, requesterID string) (bool, error) {
	// Lock the reservation data
	p.reservationLock.Lock()
	defer p.reservationLock.Unlock()

	// Check if the room exists
	valid := p.roomConfig.Contains(room)
	if !valid {
		return false, fmt.Errorf("room not found")
	}

	// Remove the reservation
	return p.removeReservation(room, date, id, requesterID)
}

func (p *Plugin) GetReservation(room string, date *AbsoluteTime, id uuid.UUID, requesterID string) (*Reservation, error) {
	// Lock the reservation data
	p.reservationLock.Lock()
	defer p.reservationLock.Unlock()

	reservationSet := ReservationSet{
		Reservations: Reservations{},
	}
	err := p.client.KV.Get(buildIndex(room, date), &reservationSet)
	if err != nil {
		return nil, err
	}

	// var targetReservation *Reservation = nil
	targetReservation := -1
	for i, reservation := range reservationSet.Reservations {
		if reservation.ID == id {
			targetReservation = i
			break
		}
	}

	if targetReservation < 0 {
		return nil, fmt.Errorf("reservation with that id not found for room and date")
	}

	reservation := reservationSet.Reservations[targetReservation]

	// Set the IsEditable attribute for the reservation
	reservation.IsEditable = checkPermission(reservation, requesterID, p.reservationAdmins)

	return &reservation, nil
}

func (p *Plugin) UpdateReservation(
	requesterID string,
	// Original reservation
	originalRoom string,
	originalDate *AbsoluteTime,
	originalID uuid.UUID,
	// New reservation
	reservation *Reservation,

) error {
	// Lock the reservation data
	p.reservationLock.Lock()
	defer p.reservationLock.Unlock()

	// Check if the room exists
	valid := p.roomConfig.Contains(reservation.Room)
	if !valid {
		return fmt.Errorf("room not found")
	}

	// Check the reservation data
	err := reservation.CheckData()
	if err != nil {
		return fmt.Errorf("could not check reservation validity: %w", err)
	}

	// Check for overlapping reservations
	hasOverlap, err := p.checkOverlap(reservation, &originalID)
	if err != nil {
		return fmt.Errorf("error checking for overlapping reservations: %w", err)
	}
	if hasOverlap {
		return fmt.Errorf("reservation overlaps with existing reservation")
	}

	// Remove the original reservation
	ok, err := p.removeReservation(originalRoom, originalDate, originalID, requesterID)
	if err != nil || !ok {
		return fmt.Errorf("error removing original reservation")
	}

	// Add the new reservation
	return p.addReservation(reservation)
}

func (p *Plugin) addReservation(reservation *Reservation) error {
	// Load the reservation data
	index, reservationSet, err := p.loadReservationSet(reservation.Room, &reservation.Start)
	if err != nil {
		return fmt.Errorf("reservation set could not be retrieved: %w", err)
	}

	reservationSet.Reservations = append(reservationSet.Reservations, *reservation)

	// Save the updated reservation set
	reservationData, err := json.Marshal(reservationSet)
	if err != nil {
		return err
	}

	expiry := AbsoluteTimeNow().Until(reservation.End) + time.Hour*24*7
	options := pluginapi.SetExpiry(expiry)

	_, err = p.client.KV.Set(index, reservationData, options)
	if err != nil {
		return err
	}

	return nil
}

func (p *Plugin) removeReservation(room string, date *AbsoluteTime, id uuid.UUID, requesterID string) (bool, error) {
	// Load the reservation data
	index, reservationSet, err := p.loadReservationSet(room, date)
	if err != nil {
		return false, fmt.Errorf("reservation set could not be retrieved: %w", err)
	}

	// Look for reservations with that id
	targetIndex := -1
	for index, reservation := range reservationSet.Reservations {
		if reservation.ID == id {
			targetIndex = index
			break
		}
	}

	if targetIndex < 0 {
		return false, fmt.Errorf("reservation with that id not found for room and date")
	}

	allowed := checkPermission(reservationSet.Reservations[targetIndex], requesterID, p.reservationAdmins)
	if !allowed {
		return false, fmt.Errorf("user does not have permission to remove reservation")
	}

	// Remove reservation if found
	reservationSet.Reservations = append(reservationSet.Reservations[:targetIndex], reservationSet.Reservations[targetIndex+1:]...)

	// Save the updated reservation set
	reservationData, err := json.Marshal(reservationSet)
	if err != nil {
		return false, err
	}

	_, err = p.client.KV.Set(index, reservationData)
	if err != nil {
		return false, err
	}

	return true, nil
}

// Load the reservation data
func (p *Plugin) loadReservationSet(room string, start *AbsoluteTime) (string, *ReservationSet, error) {
	// Load the existing reservation data
	index := buildIndex(room, start)

	reservationSet := ReservationSet{
		Reservations: Reservations{},
	}

	err := p.client.KV.Get(index, &reservationSet)
	if err != nil {
		return "", nil, err
	}

	return index, &reservationSet, nil
}

// Check if the reservation overlaps with existing reservations
func (p *Plugin) checkOverlap(newReservation *Reservation, ignoreID *uuid.UUID) (bool, error) {
	// Load the reservation data
	_, reservationSet, err := p.loadReservationSet(newReservation.Room, &newReservation.Start)
	if err != nil {
		return false, fmt.Errorf("reservation set could not be retrieved: %w", err)
	}

	for _, reservation := range reservationSet.Reservations {
		if ignoreID != nil && reservation.ID == *ignoreID {
			continue
		}

		if newReservation.Start.Before(reservation.End) && newReservation.End.After(reservation.Start) {
			return true, nil
		}
	}
	return false, nil
}

// Build the index for the reservation data
func buildIndex(room string, date *AbsoluteTime) string {
	return DBPrefix + room + "-" + date.FormatDate()
}

// Check if the requester has permission to modify the reservation
func checkPermission(reservation Reservation, requesterID string, adminList []string) bool {
	if reservation.Owner == requesterID {
		return true
	}

	isAdmin := checkAdmin(adminList, requesterID)
	return isAdmin
}

func checkAdmin(adminList []string, requesterID string) bool {
	for _, adminID := range adminList {
		if requesterID == adminID {
			return true
		}
	}
	return false
}
