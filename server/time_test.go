package main

import (
	"encoding/json"
	"fmt"
	"testing"
)

type Wrapper struct {
	Start AbsoluteTime `json:"stamp"`
	End   AbsoluteTime `json:"end"`
}

func TestToLocal(t *testing.T) {
	originalStamp := "2023-04-09T10:11:58"
	absStamp, err := NewAbsoluteTime(originalStamp)
	if err != nil {
		t.Errorf("error: %v", err)
	}

	fmt.Println("absStamp: ", absStamp)

	localStamp := absStamp.ToLocal()

	fmt.Println("localStamp: ", localStamp)

	if localStamp.Format(TimeFormat) != originalStamp {
		t.Errorf("expected %v, got %v", originalStamp, localStamp.Format(TimeFormat))
	}
}

func TestAbsoluteTime_MarshalJSON(t *testing.T) {
	originalStampStart := "2023-04-09T10:11:58"
	originalStampEnd := "2023-04-09T11:11:58"

	start, err := NewAbsoluteTime(originalStampStart)
	if err != nil {
		t.Errorf("error: %v", err)
	}
	end, err := NewAbsoluteTime(originalStampEnd)
	if err != nil {
		t.Errorf("error: %v", err)
	}

	data := Wrapper{Start: start, End: end}

	jsonData, err := json.Marshal(data)
	if err != nil {
		t.Errorf("error: %v", err)
	}

	expected := fmt.Sprintf("{\"stamp\":\"%s\",\"end\":\"%s\"}", originalStampStart, originalStampEnd)

	if string(jsonData) != expected {
		t.Errorf("expected %v, got %v", expected, string(jsonData))
	}

	parsedData := Wrapper{}

	err = json.Unmarshal(jsonData, &parsedData)
	if err != nil {
		t.Errorf("error: %v", err)
	}

	if parsedData.Start.Time != originalStampStart {
		t.Errorf("expected %v, got %v", originalStampStart, parsedData.Start.Time)
	}

	if parsedData.End.Time != originalStampEnd {
		t.Errorf("expected %v, got %v", originalStampEnd, parsedData.End.Time)
	}
}
