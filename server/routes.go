package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"github.com/google/uuid"

	ics "github.com/arran4/golang-ical"
)

type ReservationPostRequest struct {
	Room             string `json:"room"`
	ReservationStart string `json:"reservation_start"`
	ReservationEnd   string `json:"reservation_end"`
	ParticipantCount int    `json:"participant_count"`
	Description      string `json:"description"`
}

func (p *Plugin) handleAuth(w http.ResponseWriter, r *http.Request) {
	queryToken := r.URL.Query().Get("token")

	token, err := url.QueryUnescape(queryToken)
	if err != nil {
		p.client.Log.Error("error un-escaping token", "error", err)
		http.Error(w, "error un-escaping token", http.StatusInternalServerError)
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:     "mm-reservation-plugin-token",
		Value:    token,
		HttpOnly: true,
	})

	// Redirect to the plugin's index page
	http.Redirect(w, r, fmt.Sprintf("%s%s/app/", p.serverURL, p.pluginPath), http.StatusFound)
}
func (p *Plugin) handleKioskAuth(w http.ResponseWriter, r *http.Request) {
	queryToken := r.URL.Query().Get("token")

	token, err := url.QueryUnescape(queryToken)
	if err != nil {
		p.client.Log.Error("error un-escaping token", "error", err)
		http.Error(w, "error un-escaping token", http.StatusInternalServerError)
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:     "mm-reservation-plugin-token-kiosk",
		Value:    token,
		HttpOnly: false, // Allow the frontend to access the token via JS. IMPROVE: set the actual token in a cookie using HttpOnly true and set a simple second cookie (containing e.g. value `true`) for the frontend using HttpOnly false
	})

	// Redirect to the plugin's index page
	http.Redirect(w, r, fmt.Sprintf("%s%s/app/", p.serverURL, p.pluginPath), http.StatusFound)
}

func (p *Plugin) handleReservationGet(w http.ResponseWriter, req *http.Request) {
	// Get the date from the request query
	dateQuery := req.URL.Query().Get("date")
	date, err := NewAbsoluteDate(dateQuery)
	if err != nil {
		p.client.Log.Error("error parsing date", "error", err)
		p.writeError(w, err, "error parsing date", http.StatusBadRequest)
		return
	}

	// Get the requester from the context
	requestUser := req.Context().Value(AuthUserKey).(*AuthUser)

	reservationList, err := p.GetReservationList(&date, requestUser)
	if err != nil {
		p.client.Log.Error("error getting reservation list", "error", err)
		p.writeError(w, err, "error getting reservation list", http.StatusInternalServerError)
		return
	}

	// Replace user IDs with usernames
	for roomID, room := range reservationList.Rooms {
		for reservationID, reservation := range room.Reservations {
			owner := reservation.Owner

			user, err := p.client.User.Get(owner)
			if err != nil {
				p.client.Log.Error("error getting user", "error", err)
				p.writeError(w, err, "error getting user", http.StatusInternalServerError)
				return
			}
			reservationList.Rooms[roomID].Reservations[reservationID].Owner = user.Username
		}
	}

	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(reservationList)
	if err != nil {
		p.client.Log.Error("error encoding response", "error", err)
		p.writeError(w, err, "error encoding response", http.StatusInternalServerError)
		return
	}
}

func (p *Plugin) handleReservationPost(w http.ResponseWriter, r *http.Request) {
	// Get the requester from the context
	requestUser := r.Context().Value(AuthUserKey).(*AuthUser)

	var requestBody ReservationPostRequest
	err := json.NewDecoder(r.Body).Decode(&requestBody)
	if err != nil {
		p.client.Log.Error("error decoding request body", "error", err)
		p.writeError(w, err, "error decoding request body", http.StatusBadRequest)
		return
	}

	// Parse the start time
	start, err := NewAbsoluteTime(requestBody.ReservationStart)
	if err != nil {
		p.client.Log.Error("error parsing start time", "error", err)
		p.writeError(w, err, "error parsing start time", http.StatusBadRequest)
		return
	}

	// Parse the end time
	end, err := NewAbsoluteTime(requestBody.ReservationEnd)
	if err != nil {
		p.client.Log.Error("error parsing end time", "error", err)
		p.writeError(w, err, "error parsing end time", http.StatusBadRequest)
		return
	}

	reservation := Reservation{
		ID:               uuid.New(),
		Room:             requestBody.Room,
		Start:            start,
		End:              end,
		Owner:            requestUser.Id,
		ParticipantCount: requestBody.ParticipantCount,
		Description:      requestBody.Description,
	}

	err = p.AddReservation(&reservation)
	if err != nil {
		p.client.Log.Error("error adding reservation", "error", err)
		p.writeError(w, err, "error adding reservation", http.StatusConflict)
		return
	}

	err = p.notifyNewReservation(&reservation)
	if err != nil {
		p.client.Log.Error("error notifying new reservation", "error", err)
	}

	if err := p.addUserToDoorLockChannel(&reservation); err != nil {
		p.client.Log.Error("error adding user to the door lock channel", "error", err)
	}

	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(reservation)
	if err != nil {
		p.client.Log.Error("error encoding response", "error", err)
		p.writeError(w, err, "error encoding response", http.StatusInternalServerError)
		return
	}
}

func (p *Plugin) handleReservationPut(w http.ResponseWriter, req *http.Request) {
	// Get the requester from the context
	requestUser := req.Context().Value(AuthUserKey).(*AuthUser)

	// Get the old reservation data from the request query
	oldDateQuery := req.URL.Query().Get("date")
	oldIDQuery := req.URL.Query().Get("id")
	oldRoom := req.URL.Query().Get("room")

	// Parse the old date
	oldDate, err := NewAbsoluteDate(oldDateQuery)
	if err != nil {
		p.client.Log.Error("error parsing date", "error", err)
		p.writeError(w, err, "error parsing date", http.StatusBadRequest)
		return
	}

	// Parse the old ID
	oldID, err := uuid.Parse(oldIDQuery)
	if err != nil {
		p.client.Log.Error("error parsing id", "error", err)
		p.writeError(w, err, "error parsing id", http.StatusBadRequest)
		return
	}

	// Parse the request body containing the new reservation data
	var requestBody ReservationPostRequest
	err = json.NewDecoder(req.Body).Decode(&requestBody)
	if err != nil {
		p.client.Log.Error("error decoding request body", "error", err)
		p.writeError(w, err, "error decoding request body", http.StatusBadRequest)
		return
	}

	// Parse the start time
	start, err := NewAbsoluteTime(requestBody.ReservationStart)
	if err != nil {
		p.client.Log.Error("error parsing start time", "error", err)
		p.writeError(w, err, "error parsing start time", http.StatusBadRequest)
		return
	}

	// Parse the end time
	end, err := NewAbsoluteTime(requestBody.ReservationEnd)
	if err != nil {
		p.client.Log.Error("error parsing end time", "error", err)
		p.writeError(w, err, "error parsing end time", http.StatusBadRequest)
		return
	}

	reservation := Reservation{
		ID:               uuid.New(),
		Room:             requestBody.Room,
		Start:            start,
		End:              end,
		Owner:            requestUser.Id,
		ParticipantCount: requestBody.ParticipantCount,
		Description:      requestBody.Description,
	}

	err = p.UpdateReservation(
		requestUser.Id,
		oldRoom,
		&oldDate,
		oldID,
		&reservation,
	)
	if err != nil {
		p.client.Log.Error("error updating reservation", "error", err)
		p.writeError(w, err, "error updating reservation", http.StatusConflict)
		return
	}

	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(reservation)
	if err != nil {
		p.client.Log.Error("error encoding response", "error", err)
		p.writeError(w, err, "error encoding response", http.StatusInternalServerError)
		return
	}
}

func (p *Plugin) handleReservationDelete(w http.ResponseWriter, req *http.Request) {
	// Get the requester from the context
	requestUser := req.Context().Value(AuthUserKey).(*AuthUser)

	dateQuery := req.URL.Query().Get("date")
	IDQuery := req.URL.Query().Get("id")
	room := req.URL.Query().Get("room")

	date, err := NewAbsoluteDate(dateQuery)
	if err != nil {
		p.client.Log.Error("error parsing date", "error", err)
		p.writeError(w, err, "error parsing date", http.StatusBadRequest)
		return
	}

	id, err := uuid.Parse(IDQuery)
	if err != nil {
		p.client.Log.Error("error parsing id", "error", err)
		p.writeError(w, err, "error parsing id", http.StatusBadRequest)
		return
	}

	ok, err := p.RemoveReservation(room, &date, id, requestUser.Id)
	if err != nil {
		p.client.Log.Error("error remove reservation", "error", err)
		p.writeError(w, err, "error remove reservation", http.StatusInternalServerError)
		return
	}

	type Response struct {
		Ok bool `json:"ok"`
	}

	response := Response{
		Ok: ok,
	}
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(response)
	if err != nil {
		p.client.Log.Error("error encoding response", "error", err)
		p.writeError(w, err, "error encoding response", http.StatusInternalServerError)
		return
	}
}

func (p *Plugin) handleReservationExport(w http.ResponseWriter, req *http.Request) {
	// Get the requester from the context

	dateQuery := req.URL.Query().Get("date")
	IDQuery := req.URL.Query().Get("id")
	room := req.URL.Query().Get("room")

	date, err := NewAbsoluteDate(dateQuery)
	if err != nil {
		p.client.Log.Error("error parsing date", "error", err)
		p.writeError(w, err, "error parsing date", http.StatusBadRequest)
		return
	}

	id, err := uuid.Parse(IDQuery)
	if err != nil {
		p.client.Log.Error("error parsing id", "error", err)
		p.writeError(w, err, "error parsing id", http.StatusBadRequest)
		return
	}

	// Get the requester from the context
	requestUser := req.Context().Value(AuthUserKey).(*AuthUser)

	reservation, err := p.GetReservation(room, &date, id, requestUser.Id)
	if err != nil {
		p.client.Log.Error("error get reservation", "error", err)
		p.writeError(w, err, "error get reservation", http.StatusInternalServerError)
		return
	}

	// Convert to ics
	cal, err := p.buildICalEvent(reservation)
	if err != nil {
		p.client.Log.Error("error building ical", "error", err)
		p.writeError(w, err, "error building ical", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "text/calendar")
	w.Header().Set("Content-Disposition", "attachment; filename=reservation.ics")
	_, err = w.Write([]byte(cal.Serialize()))
	if err != nil {
		p.client.Log.Error("error writing ical", "error", err)
	}
}

func (p *Plugin) handleNotFound(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
	fmt.Fprintf(w, "Not found: %s %s", r.Method, r.URL.Path)
}

type APIError struct {
	Error   string `json:"error"`
	Message string `json:"message"`
}

func (p *Plugin) writeError(w http.ResponseWriter, err error, message string, code int) {
	// write error
	w.WriteHeader(code)
	resp := APIError{
		Error:   err.Error(),
		Message: message,
	}

	err = json.NewEncoder(w).Encode(resp)
	if err != nil {
		p.client.Log.Error("error writing error response", "error", err)
	}
}

func (p *Plugin) buildICalEvent(reservation *Reservation) (*ics.Calendar, error) {
	room, ok := p.roomConfig.GetRoom(reservation.Room)
	if !ok {
		return nil, fmt.Errorf("room not found")
	}

	cal := ics.NewCalendar()

	cal.SetMethod(ics.MethodRequest)
	cal.SetTimezoneId("UTC")

	event := cal.AddEvent(reservation.ID.String())

	event.SetOrganizer(reservation.Owner)
	event.SetStartAt(reservation.Start.ToLocal())
	event.SetEndAt(reservation.End.ToLocal())

	event.SetLocation(fmt.Sprintf("%s\n%s", p.location, room.Name))

	event.SetSummary(reservation.Description)

	return cal, nil
}

func (p *Plugin) handleFloorPlanUpload(w http.ResponseWriter, r *http.Request) {
	floorplan, _, err := r.FormFile("floorplan")
	if err != nil {
		p.client.Log.Error("error getting floorplan", "error", err)
		p.writeError(w, err, "error getting floorplan", http.StatusBadRequest)
		return
	}

	defer floorplan.Close()

	err = p.SaveFloorplan(floorplan)
	if err != nil {
		p.client.Log.Error("error saving floorplan", "error", err)
		p.writeError(w, err, "error saving floorplan", http.StatusInternalServerError)
		return
	}

	roomConfig, err := p.LoadRoomConfig()
	if err != nil {
		p.client.Log.Error("error reloading room config", "error", err)
		p.writeError(w, err, "error reloading room config", http.StatusInternalServerError)
		return
	}
	p.roomConfig = roomConfig

	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(nil)
	if err != nil {
		p.client.Log.Error("error encoding response", "error", err)
		p.writeError(w, err, "error encoding response", http.StatusInternalServerError)
		return
	}
}

func (p *Plugin) handleFloorPlanGet(w http.ResponseWriter, _ *http.Request) {
	data, err := p.LoadSavedFloorplan()
	if err != nil {
		p.client.Log.Error("error loading floorplan", "error", err)
		p.writeError(w, err, "error loading floorplan", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "image/svg+xml")
	_, err = w.Write(data)

	if err != nil {
		p.client.Log.Error("error writing floorplan", "error", err)
	}
}

// handleWelcomeGet returns the kiosk welcome text. Note: this seems to be not possible for the plugin web app / frontend to get this setting directly
func (p *Plugin) handleWelcomeGet(w http.ResponseWriter, _ *http.Request) {
	_, err := w.Write([]byte(p.configuration.KioskWelcomeText))

	if err != nil {
		p.client.Log.Error("error writing welcome text", "error", err)
	}
}
