package main

// See https://developers.mattermost.com/extend/plugins/server/reference/

import (
	"context"
	"fmt"
	"io/fs"
	"net/http"
	"sync"

	"embed"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"

	"github.com/mattermost/mattermost/server/public/plugin"
	"github.com/mattermost/mattermost/server/public/pluginapi"
)

//go:embed static/*
var EmbeddedAssets embed.FS

// Plugin implements the interface expected by the Mattermost server to communicate between the server and plugin processes.
type Plugin struct {
	plugin.MattermostPlugin

	// configurationLock synchronizes access to the configuration.
	configurationLock sync.RWMutex

	// configuration is the active plugin configuration. Consult getConfiguration and
	// setConfiguration for usage.
	configuration *configuration

	client *pluginapi.Client

	// reservationLock synchronizes access to the reservation data.
	reservationLock sync.RWMutex

	// Office location
	location string

	// roomConfig
	roomConfig *RoomConfig

	// reservationRooms
	reservationAdmins []string

	// reservationChannel
	reservationChannel string

	// doorlockChannel
	doorLockChannel string

	// BotUserID
	botUserID string

	// serverUrl
	serverURL string

	// pluginPath
	pluginPath string

	// signingKey
	signingKey []byte

	// kiosk key
	kioskKey string

	router *chi.Mux
}

var Router = chi.NewRouter()

func (p *Plugin) Print(v ...interface{}) {
	p.client.Log.Warn(fmt.Sprint(v...))
}

func (p *Plugin) InitAPI() *chi.Mux {
	pluginRouter := chi.NewRouter()

	pluginRouter.Use(middleware.Recoverer)
	pluginRouter.Use(middleware.RequestID)
	pluginRouter.Use(middleware.RequestLogger(&middleware.DefaultLogFormatter{
		Logger:  p,
		NoColor: true,
	}))
	pluginRouter.NotFound(p.handleNotFound)

	pluginRouter.Get("/auth", p.handleAuth)
	pluginRouter.Get("/kiosk-auth", p.handleKioskAuth)

	subFS, err := fs.Sub(EmbeddedAssets, "static")
	if err != nil {
		panic("missing static subdir") // IMPROVE: better error handling
	}
	pluginRouter.Handle("/app/*", http.StripPrefix("/app/", http.FileServer(http.FS(subFS))))

	// Require authentication for the routes in the following group
	pluginRouter.Group(func(r chi.Router) {
		r.Use(p.UserAuthMiddleware)

		// Require the configuration to be set for routes handling reservations
		r.Group(func(r chi.Router) {
			r.Use(p.ConfMiddleware)
			r.Get("/reservation", p.handleReservationGet)
			r.Get("/reservation/export", p.handleReservationExport)
			r.Get("/welcome", p.handleWelcomeGet) // Kiosk welcome text

			// Require editor rights for changing reservations
			r.Group(func(r chi.Router) {
				r.Use(p.CheckEditorMiddleware)
				r.Post("/reservation", p.handleReservationPost)
				r.Put("/reservation", p.handleReservationPut)
				r.Delete("/reservation", p.handleReservationDelete)
			})
		})

		r.Get("/floorplan", p.handleFloorPlanGet)

		// Require admin rights for changing the floor plan
		r.Group(func(r chi.Router) {
			r.Use(p.CheckAdminMiddleware)
			r.Post("/floorplan", p.handleFloorPlanUpload)
		})
	})

	return pluginRouter
}

func (p *Plugin) ServeHTTP(c *plugin.Context, w http.ResponseWriter, r *http.Request) {
	p.client.Log.Debug("New request:", "Host", r.Host, "RequestURI", r.RequestURI, "Method", r.Method, "path", r.URL.Path)

	// Serve via Chi, but pass the session ID from the plugin's context to the request's context
	ctx := context.WithValue(r.Context(), SessionIdKey, c.SessionId)

	p.router.ServeHTTP(w, r.WithContext(ctx))
}
