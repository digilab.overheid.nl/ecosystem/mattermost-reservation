package main

import (
	"github.com/mattermost/mattermost/server/public/model"
	"github.com/mattermost/mattermost/server/public/plugin"
)

// When a message is received, check if it is a command for the reservation bot
func (p *Plugin) MessageHasBeenPosted(_ *plugin.Context, post *model.Post) {
	// Ignore messages sent by the bot itself
	if sentByPlugin, _ := post.GetProp(ReservationBotMessageMarker).(bool); sentByPlugin {
		return
	}
	// Ignore system messages announcing the bot leaving the channel
	if post.Type == model.PostTypeRemoveFromChannel {
		return
	}

	// Get the channel the message was posted in
	channel, err := p.client.Channel.Get(post.ChannelId)
	if err != nil {
		p.client.Log.Error("error getting channel", "error", err)
		return
	}

	// Check if it is a direct message
	_, err = p.handleDirectMessage(channel, post)
	if err != nil {
		p.client.Log.Error("error handling direct message", "error", err)
	}

	// Not interested in this message
}

// Returns (handled, error):
// - handled is true if the message was handled by this function
// - error if an error occurred
func (p *Plugin) handleDirectMessage(channel *model.Channel, post *model.Post) (bool, error) {
	// Check if it is a private message
	if channel.Type != model.ChannelTypeDirect {
		// Not a direct message
		return false, nil
	}

	// Check if bot is member of the channel
	botMembers, appErr := p.client.Channel.ListMembersByIDs(post.ChannelId, []string{p.botUserID})
	if appErr != nil {
		p.client.Log.Error("error getting channel members", "error", appErr)
		return false, appErr
	}
	if len(botMembers) == 0 {
		// Bot is not a member of the channel
		return false, nil
	}

	// Check if user is member of p.reservationChannel
	members, err := p.client.Channel.ListMembersByIDs(p.reservationChannel, []string{post.UserId})
	if err != nil {
		p.client.Log.Error("error getting channel member", "error", err)
		return false, err
	}

	if len(members) == 0 {
		// User is not a member of the channel
		err = p.sendMembershipMessage(post.ChannelId)
		if err != nil {
			p.client.Log.Error("error sending membership message", "error", err)
			return false, err
		}
		return false, nil
	}

	token, err := CreateAuthToken(p.signingKey, post.UserId)
	if err != nil {
		return true, err
	}

	err = p.sendAuthMessage(post.ChannelId, token)
	if err != nil {
		return true, err
	}

	return true, nil
}
