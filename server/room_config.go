package main

import (
	"bytes"
	"io"

	"github.com/JoshVarga/svgparser"
)

type RoomConfig struct {
	Rooms map[string]Room
}

type Room struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

func (c *RoomConfig) Contains(id string) bool {
	_, ok := c.Rooms[id]
	return ok
}

func (c *RoomConfig) GetRoom(id string) (Room, bool) {
	room, ok := c.Rooms[id]
	return room, ok
}

func (p *Plugin) LoadRoomConfig() (*RoomConfig, error) {
	data, err := p.LoadSavedFloorplan()
	if err != nil {
		return nil, err
	}

	roomConfig, err := ParseSvgFile(data)
	if err != nil {
		return nil, err
	}

	return roomConfig, nil
}

var FloorplanKey = "floorplan-id"

// Save the svg data to the file store and save the file id to the KV store
func (p *Plugin) SaveFloorplan(floorplan io.Reader) error {
	fileInfo, err := p.client.File.Upload(floorplan, "floorplan.svg", p.reservationChannel)
	if err != nil {
		return err
	}

	_, err = p.client.KV.Set(FloorplanKey, fileInfo.Id)
	if err != nil {
		return err
	}

	return nil
}

// Return the saved svg data
func (p *Plugin) LoadSavedFloorplan() ([]byte, error) {
	fileID := ""

	err := p.client.KV.Get(FloorplanKey, &fileID)
	if err != nil {
		return nil, err
	}

	dataReader, err := p.client.File.Get(fileID)
	if err != nil {
		return nil, err
	}

	data, err := io.ReadAll(dataReader)
	if err != nil {
		return nil, err
	}

	return data, nil
}

// Parse the svg file and return the contained room configuration
func ParseSvgFile(data []byte) (*RoomConfig, error) {
	roomConfig := RoomConfig{
		Rooms: make(map[string]Room),
	}

	reader := bytes.NewReader(data)

	elem, err := svgparser.Parse(reader, false)
	if err != nil {
		return nil, err
	}

	rooms := FindRooms(elem)
	for _, room := range rooms {
		id := room.Attributes["id"]
		name := room.Attributes["data-name"]

		roomConfig.Rooms[id] = Room{
			ID:   id,
			Name: name,
		}
	}

	return &roomConfig, nil
}

// FindAll finds all children with the required attributes.
func FindRooms(element *svgparser.Element) []*svgparser.Element {
	var elements []*svgparser.Element
	for _, child := range element.Children {
		_, hasID := child.Attributes["id"]
		_, hasName := child.Attributes["data-name"]

		if hasID && hasName {
			elements = append(elements, child)
		}
		elements = append(elements, FindRooms(child)...)
	}
	return elements
}
