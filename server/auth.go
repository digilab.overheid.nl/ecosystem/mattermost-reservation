package main

import (
	"context"
	"fmt"
	"net/http"
	"slices"
	"time"

	"github.com/google/uuid"
	"github.com/mattermost/mattermost/server/public/model"
	"github.com/o1egl/paseto/v2"
)

type AuthType string

const (
	AuthTypeReader AuthType = "reader"
	AuthTypeEditor AuthType = "editor"
	AuthTypeAdmin  AuthType = "admin"
)

func (a AuthType) IsEditor() bool {
	return a == AuthTypeEditor || a == AuthTypeAdmin
}

func (a AuthType) IsAdmin() bool {
	return a == AuthTypeAdmin
}

// The AuthUser does not contain any user data for users with userType AuthTypeReader
type AuthUser struct {
	*model.User
	userType AuthType
}

func newAuthUser(u *model.User, t AuthType) *AuthUser {
	return &AuthUser{
		User:     u,
		userType: t,
	}
}

func CreateAuthToken(symmetricKey []byte, ident string) (string, error) {
	now := time.Now()
	exp := now.Add(time.Hour)
	nbt := now

	jsonToken := paseto.JSONToken{
		Audience:   "mm-reservation-plugin",
		Issuer:     "mm-reservation-plugin",
		Jti:        uuid.NewString(),
		Subject:    ident,
		IssuedAt:   now,
		Expiration: exp,
		NotBefore:  nbt,
	}

	// Encrypt data
	token, err := paseto.Encrypt(symmetricKey, jsonToken, "_footer_")
	if err != nil {
		return "", err
	}

	return token, nil
}

func (p *Plugin) doKioskAuth(r *http.Request) (bool, error) {
	tokenCookie, err := r.Cookie("mm-reservation-plugin-token-kiosk")
	isKiosk := false

	if err == http.ErrNoCookie {
		return isKiosk, nil
	}
	isKiosk = true
	if err != nil {
		p.client.Log.Error("kiosk: error getting token from cookie", "error", err)
		return isKiosk, err
	}

	if tokenCookie == nil || tokenCookie.Value == "" {
		p.client.Log.Error("kiosk: no token found in cookie")
		return isKiosk, fmt.Errorf("kiosk: no token found in cookie")
	}
	token := tokenCookie.Value

	if token != p.kioskKey && p.kioskKey != "" {
		p.client.Log.Error("kiosk: invalid kiosk key")
		return isKiosk, fmt.Errorf("kiosk: invalid kiosk key")
	}

	return isKiosk, nil
}

func (p *Plugin) doTokenAuth(r *http.Request) (*model.User, error) {
	ctx := r.Context()

	// Get the user ID
	var userID string

	// Method 1: get the Mattermost session from the request context
	sessionID, _ := ctx.Value(SessionIdKey).(string)
	session, appErr := p.API.GetSession(sessionID)
	if appErr == nil {
		// Ensure that the user is a system admin or belongs to the configured reservation channel
		if slices.Contains(session.GetUserRoles(), model.SystemAdminRoleId) {
			userID = session.UserId
		} else {
			members, err := p.client.Channel.ListMembersByIDs(p.reservationChannel, []string{session.UserId})
			if err == nil && len(members) != 0 {
				userID = session.UserId
			}
		}
	}

	if userID == "" {
		// Method 2: get token from cookie
		tokenCookie, err := r.Cookie("mm-reservation-plugin-token")
		if err != nil {
			p.client.Log.Error("error getting token from cookie", "error", err)
			return nil, fmt.Errorf("error getting token from cookie")
		}
		if tokenCookie == nil || tokenCookie.Value == "" {
			p.client.Log.Error("no token found in cookie")
			return nil, fmt.Errorf("no token found in cookie")
		}
		token := tokenCookie.Value

		// Decrypt token
		var authToken paseto.JSONToken
		var newFooter string
		err = paseto.Decrypt(token, p.signingKey, &authToken, &newFooter)
		if err != nil {
			p.client.Log.Error("error decrypting token", "error", err)
			return nil, fmt.Errorf("error decrypting token")
		}

		// Validate token
		err = authToken.Validate()
		if err != nil {
			p.client.Log.Error("error validating token", "error", err)
			return nil, fmt.Errorf("error validating token")
		}

		userID = authToken.Subject
	}

	// Find the user
	user, err := p.client.User.Get(userID)
	if err != nil {
		p.client.Log.Error("error getting user", "error", err)
		return nil, fmt.Errorf("error getting user")
	}

	return user, nil
}

func (p *Plugin) UserAuthMiddleware(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		var user *model.User
		var userType AuthType

		isKiosk, err := p.doKioskAuth(r)
		if err != nil {
			http.Error(w, "error authenticating (kiosk) user", http.StatusInternalServerError)
			return
		}

		if isKiosk {
			userType = AuthTypeReader
		} else {
			tokenUser, err := p.doTokenAuth(r)
			if err != nil {
				http.Error(w, "error authenticating (editor) user", http.StatusInternalServerError)
				return
			}
			user = tokenUser

			if checkAdmin(p.reservationAdmins, user.Id) {
				userType = AuthTypeAdmin
			} else {
				userType = AuthTypeEditor
			}
		}

		// Add the user pointer to the context
		ctx = context.WithValue(ctx, AuthUserKey, newAuthUser(user, userType))
		next.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(fn)
}

func (p *Plugin) CheckAdminMiddleware(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		user, ok := r.Context().Value(AuthUserKey).(*AuthUser)
		if !ok {
			p.client.Log.Error("error getting user from context")
			p.writeError(w, nil, "error getting user from context", http.StatusInternalServerError)
			return
		}

		// Check if the user is a Mattermost system admin or belongs to the group of admins
		isAdmin := user.IsSystemAdmin() || user.userType.IsAdmin()
		if !isAdmin {
			p.writeError(w, nil, "Access denied", http.StatusForbidden)
			return
		}

		next.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(fn)
}

func (p *Plugin) CheckEditorMiddleware(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		user, ok := r.Context().Value(AuthUserKey).(*AuthUser)
		if !ok {
			p.client.Log.Error("error getting user from context")
			p.writeError(w, nil, "error getting user from context", http.StatusInternalServerError)
			return
		}

		if !user.userType.IsEditor() {
			p.writeError(w, nil, "Access denied", http.StatusForbidden)
			return
		}

		next.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(fn)
}

func (p *Plugin) ConfMiddleware(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		if p.roomConfig == nil {
			p.writeError(w, nil, "Room configuration not ready", http.StatusServiceUnavailable)
			return
		}

		next.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(fn)
}

type ctxKey int

const AuthUserKey ctxKey = 0

// SessionIdKey, used as key to pass the session ID from the plugin context to the router's request context
const SessionIdKey ctxKey = 1
