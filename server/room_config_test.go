package main

import (
	"fmt"
	"os"
	"testing"
)

func TestParseSvgFileLarge(t *testing.T) {
	file, err := os.ReadFile("./test/plattegrond_digilab_plain.svg")
	if err != nil {
		t.Fatal(err)
	}

	conf, err := ParseSvgFile(file)
	if err != nil {
		t.Fatal(err)
	}

	fmt.Printf("%+v\n", conf)

	expected := map[string]Room{
		"room601": {"room601", "Limbo"},
		"room602": {"room602", "Tante Sidonias Theelepel"},
		"room603": {"room603", "Panoptinon"},
		"blok001": {"blok001", "Tafel blok 1"},
		"blok002": {"blok002", "Tafel blok 2"},
		"blok003": {"blok003", "Tafel blok 3"},
		"blok004": {"blok004", "Tafel blok 4"},
	}

	if len(conf.Rooms) != len(expected) {
		t.Fatal("Expected 3 rooms, got ", len(conf.Rooms))
	}

	for k, v := range conf.Rooms {
		if expected[k] != v {
			t.Fatalf("Expected %+v, got %+v", expected[k], v)
		}
	}
}

func TestParseSvgFile(t *testing.T) {
	file, err := os.ReadFile("./test/floor.svg")
	if err != nil {
		t.Fatal(err)
	}

	conf, err := ParseSvgFile(file)
	if err != nil {
		t.Fatal(err)
	}

	fmt.Printf("%+v\n", conf)

	expected := map[string]Room{
		"room601":   {"room601", "Limbo"},
		"room602":   {"room602", "Tante Sidonias Theelepel"},
		"room603":   {"room603", "Panoptinon"},
		"island001": {"island001", "Tafel"},
		"island002": {"island002", "Tafel"},
		"island003": {"island003", "Tafel"},
		"island004": {"island004", "Tafel"},
	}

	if len(conf.Rooms) != len(expected) {
		t.Fatal("Expected 9 rooms, got ", len(conf.Rooms))
	}

	for k, v := range conf.Rooms {
		if expected[k] != v {
			t.Fatalf("Expected %+v, got %+v", expected[k], v)
		}
	}
}
