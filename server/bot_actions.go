package main

import (
	"fmt"
	"net/url"

	"github.com/mattermost/mattermost/server/public/model"
)

func (p *Plugin) notifyNewReservation(reservation *Reservation) error {
	room, _ := p.roomConfig.GetRoom(reservation.Room)
	user, err := p.client.User.Get(reservation.Owner)
	if err != nil {
		return err
	}

	message := fmt.Sprintf("Nieuwe reservering door @%s : `%s` @ `%s` - `%s`",
		user.Username,
		room.Name,
		reservation.Start.ToLocal().Format(TimeFormat),
		reservation.End.ToLocal().Format(TimeFormat),
	)

	// Notify the channel that a new reservation was made
	err = p.sendBotMessage(p.reservationChannel, message, model.StringInterface{})
	if err != nil {
		return err
	}

	return nil
}

func (p *Plugin) addUserToDoorLockChannel(reservation *Reservation) error {
	if _, err := p.client.Channel.AddUser(p.doorLockChannel, reservation.Owner, p.botUserID); err != nil {
		return err
	}
	return nil
}

func (p *Plugin) sendAuthMessage(channelID string, token string) error {
	targetURL := p.serverURL + p.pluginPath + "/auth"
	link := fmt.Sprintf("%s?token=%s", targetURL, url.QueryEscape(token))
	message := fmt.Sprintf(":wave: [Klik hier om een ruimte te reserveren](%s)", link)

	_, err := p.sendUserMessage(p.botUserID, channelID, message, model.StringInterface{})
	return err
}

func (p *Plugin) sendMembershipMessage(channelID string) error {
	message := "Je moet lid zijn van het reserveringskanaal om een ruimte te kunnen reserveren."

	_, err := p.sendUserMessage(p.botUserID, channelID, message, model.StringInterface{})
	return err
}

// Send a message as the specified user to the specified channel
func (p *Plugin) sendUserMessage(userID string, channelID string, message string, props model.StringInterface) (*model.Post, error) {
	newPost := &model.Post{
		UserId:    userID,
		ChannelId: channelID,
		Message:   message,
	}

	props[ReservationBotMessageMarker] = true

	newPost.SetProps(props)

	err := p.client.Post.CreatePost(newPost)
	if err != nil {
		p.client.Log.Error("error sending message", "error", err)
		return nil, err
	}

	return newPost, nil
}

func (p *Plugin) sendBotMessage(channelID string, message string, props model.StringInterface) error {
	_, err := p.sendUserMessage(p.botUserID, channelID, message, props)
	return err
}

var ReservationBotMessageMarker = "reservationBotMessage"
