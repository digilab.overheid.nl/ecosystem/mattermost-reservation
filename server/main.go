package main

import (
	"github.com/mattermost/mattermost/server/public/plugin"
)

func main() {
	reservationPlugin := &Plugin{}

	plugin.ClientMain(reservationPlugin)
}

func (p *Plugin) OnActivate() error {
	p.router = p.InitAPI()

	// roomConfig, err := LoadEmbeddedRoomConfig()
	// if err != nil {
	// 	return err
	// }

	// p.roomConfig = roomConfig
	return nil
}
