package main

import (
	"encoding/json"
	"time"
)

// We don't want to include the timezone in the format, as the reservations are always at absolute times
const TimeFormat = "2006-01-02T15:04:05"
const DateFormat = "2006-01-02"

type AbsoluteTime struct {
	Time string `json:"time"`
}

func NewAbsoluteTime(input string) (AbsoluteTime, error) {
	_, err := time.Parse(TimeFormat, input)
	if err != nil {
		return AbsoluteTime{}, err
	}

	return AbsoluteTime{Time: input}, nil
}

func NewAbsoluteDate(input string) (AbsoluteTime, error) {
	input += "T00:00:00"
	_, err := time.Parse(TimeFormat, input)
	if err != nil {
		return AbsoluteTime{}, err
	}

	return AbsoluteTime{Time: input}, nil
}

func AbsoluteTimeNow() AbsoluteTime {
	stamp := time.Now().Format(TimeFormat)
	return AbsoluteTime{Time: stamp}
}

func (t AbsoluteTime) After(t2 AbsoluteTime) bool {
	tp1 := t.ToLocal()
	tp2 := t2.ToLocal()
	return tp1.After(tp2)
}
func (t AbsoluteTime) Before(t2 AbsoluteTime) bool {
	tp1 := t.ToLocal()
	tp2 := t2.ToLocal()
	return tp1.Before(tp2)
}
func (t AbsoluteTime) Sub(t2 AbsoluteTime) time.Duration {
	tp1 := t.ToLocal()
	tp2 := t2.ToLocal()
	return tp1.Sub(tp2)
}
func (t AbsoluteTime) Until(t2 AbsoluteTime) time.Duration {
	tp1 := t.ToLocal()
	tp2 := t2.ToLocal()
	return tp2.Sub(tp1)
}

func (t AbsoluteTime) FormatDate() string {
	return t.ToLocal().Format(DateFormat)
}

func (t AbsoluteTime) ToLocal() time.Time {
	stamp, err := time.Parse(TimeFormat, t.Time)
	if err != nil {
		panic("Invalid date, should be caught by constructor error: " + err.Error())
	}

	return stamp
}

func (t AbsoluteTime) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.Time)
}

func (t *AbsoluteTime) UnmarshalJSON(data []byte) error {
	tp := AbsoluteTime{}
	err := json.Unmarshal(data, &tp.Time)
	if err != nil {
		return err
	}
	*t = tp
	return nil
}
