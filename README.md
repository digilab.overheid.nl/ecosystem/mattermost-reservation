# Mattermost Reservation plugin

## Installation

1. Clone the repository to your local machine.
2. Navigate to the project directory in your terminal.
3. Run `make dist` to build the plugin. This will generate a .tar.gz file.
4. Log in to your Mattermost server.
5. Go to **System Console > Plugin Management**.
6. Ensure that your Mattermost configuration allows plugin uploads.
7. Click **Upload Plugin** and select the .tar.gz file you generated earlier.
8. After the upload completes, click **Activate** to start using the Reservation plugin.

## Configuration
1. Set your office location, this will be added to ical exports
2. Set your admins, these can edit all meetings
3. Set the kiosk auth key
4. Set the Reservation channel ID
    - Only members of this channel will be able to access the reservations system 
    - Members of the channel will be notified when reservations are created
5. Upload a SVG image as floor plan
    - Rooms in the svg should have the following format: `<rect id="room001" data-name="Main meeting room" data-max-occupants="15" data-tags="Optional comma seperated tags" />`

## Usage
1. Send a message to the @reservation-bot
2. The bot will reply with a link, this link is valid for 1 hour
3. Click on the link to open the reservation page

## Kiosk Usage
1. Let the kiosk open the page to set the kiosk auth cookie `http://localhost:8065/plugins/mattermost-reservation/kiosk-auth?token=<KIOSK_AUTH_KEY>`
2. Let the kiosk display the page `http://localhost:8065/plugins/mattermost-reservation/app/` (to which is automatically redirects)

(Where `http://localhost:8065` may be replaced with the Mattermost deployment URL.)
