#!/bin/bash

mkdir -p /tmp/mm-volumes/app/mattermost/config
mkdir -p /tmp/mm-volumes/app/mattermost/data
mkdir -p /tmp/mm-volumes/app/mattermost/logs
mkdir -p /tmp/mm-volumes/app/mattermost/plugins
mkdir -p /tmp/mm-volumes/app/mattermost/client/plugins
mkdir -p /tmp/mm-volumes/app/mattermost/bleve-indexes

sudo cp ./config.json /tmp/mm-volumes/app/mattermost/config/config.json

sudo chown -R 2000:2000 /tmp/mm-volumes/app/mattermost