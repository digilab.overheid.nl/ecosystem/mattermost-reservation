import adapter from '@sveltejs/adapter-static';
import { vitePreprocess } from '@sveltejs/vite-plugin-svelte';

/** @type {import('@sveltejs/kit').Config} */
const config = {
    // Consult https://kit.svelte.dev/docs/integrations#preprocessors
    // for more information about preprocessors
    preprocess: vitePreprocess(),

    kit: {
        // adapter-auto only supports some environments, see https://kit.svelte.dev/docs/adapter-auto for a list.
        // If your environment is not supported or you settled on a specific environment, switch out the adapter.
        // See https://kit.svelte.dev/docs/adapters for more information about adapters.
        adapter: adapter({ pages: '../server/static' }),
        paths: {
            base: '/plugins/mattermost-reservation/app', // For deployment in a subdir. Note: when Mattermost is deployed in a subdir, this base path is incorrect, but this still seems to work fine. IMPROVE: set the base URL as (build time) environment variable?
        },
    },
};

export default config;
