import {PureComponent, createRef} from 'react';
import PropTypes from 'prop-types';
import {Client4} from 'mattermost-redux/client';

import {id as pluginID} from '@/manifest';

export default class FloorplanSetting extends PureComponent {
    // See https://developers.mattermost.com/integrate/plugins/best-practices/ for a description of the props below
    static propTypes = {
        id: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,
        helpText: PropTypes.node,
        value: PropTypes.any,
        disabled: PropTypes.bool.isRequired,
        config: PropTypes.object.isRequired,
        license: PropTypes.object.isRequired,
        setByEnv: PropTypes.bool.isRequired,
        onChange: PropTypes.func.isRequired,
        setSaveNeeded: PropTypes.func.isRequired,
        registerSaveAction: PropTypes.func.isRequired,
        unRegisterSaveAction: PropTypes.func.isRequired,
    };

    inputRef = createRef();

    constructor(props) {
        super(props);

        this.state = {
            value: this.props.value,
        };

        // When the form is saved, update the image if changed
        props.registerSaveAction(() => {
            const file = this.inputRef.current.files[0];

            if (file) {
                const formData = new FormData();
                formData.append('floorplan', file);

                return fetch(`${this.getPluginServerRoute()}/floorplan`, Client4.getOptions({
                    method: 'POST',
                    body: formData,
                }));

                // IMPROVE: refresh the image, e.g. by appending `?timestamp=${new Date().getTime()}` to the src
            }

            return new Promise((resolve) => {
                resolve('nothing to be done');
            });
        });
    }

    getPluginServerRoute = () => {
        let basePath = '';
        const siteURL = this.props.config.ServiceSettings.SiteURL;

        if (siteURL) {
            basePath = new URL(siteURL).pathname;

            if (basePath && basePath[basePath.length - 1] === '/') {
                basePath = basePath.substring(0, basePath.length - 1);
            }
        }

        return basePath + '/plugins/' + pluginID;
    };

    handleChange = (e) => {
        this.setState({value: e.target.value});

        this.props.onChange(this.props.id, e.target.value);
        this.props.setSaveNeeded();
    };

    render() {
        return (
            <div className='form-group'>
                <label className='control-label col-sm-4'>{this.props.label}</label>
                <div className='col-sm-8'>
                    <div>
                        <img
                            style={style.img}
                            src={`${this.getPluginServerRoute()}/floorplan`}
                            alt='Plattegrond'
                            onError={({currentTarget}) => {
                                currentTarget.setAttribute('style', 'display: none');
                            }}
                        />
                    </div>
                    <div className='file__upload'>
                        <button
                            type='button'
                            className='btn btn-tertiary'
                        >{'Choose file'}</button>
                        <input
                            ref={this.inputRef}
                            onChange={this.handleChange}
                            type='file'
                            accept='.svg'
                        />
                    </div>
                </div>
            </div>
        );
    }
}

const style = {
    img: {
        'background-color': '#fff',
        'margin-bottom': '0.5rem',
    },
};
