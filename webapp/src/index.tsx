import {Store, Action} from 'redux';

import {GlobalState} from '@mattermost/types/lib/store';

import {id as pluginID} from '@/manifest';

import {PluginRegistry} from '@/types/mattermost-webapp';

import FloorplanSetting from './components/floorplan_setting';

export default class Plugin {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars, @typescript-eslint/no-empty-function
    public async initialize(registry: PluginRegistry, store: Store<GlobalState, Action<Record<string, unknown>>>) {
        // @see https://developers.mattermost.com/extend/plugins/webapp/reference/
        registry.registerAdminConsoleCustomSetting('floorplanSetting', FloorplanSetting);
    }
}

declare global {
    interface Window {
        registerPlugin(pluginId: string, plugin: Plugin): void
    }
}

window.registerPlugin(pluginID, new Plugin());
